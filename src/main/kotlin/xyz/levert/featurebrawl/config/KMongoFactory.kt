package xyz.levert.featurebrawl.config

import com.mongodb.MongoClientSettings
import com.mongodb.client.MongoClient
import com.mongodb.client.MongoDatabase
import io.micronaut.context.annotation.Bean
import io.micronaut.context.annotation.Factory
import io.micronaut.context.annotation.Primary
import org.bson.codecs.configuration.CodecRegistry
import org.litote.kmongo.service.ClassMappingType

@Factory
class KMongoFactory {

    @Bean
    fun codecRegistry(): CodecRegistry {
        return ClassMappingType.codecRegistry(
            MongoClientSettings.getDefaultCodecRegistry()
        )
    }

    @Bean
    @Primary
    fun mongoDatabase(client: MongoClient): MongoDatabase {
        return client.getDatabase("featureBrawlDB")
    }
}
