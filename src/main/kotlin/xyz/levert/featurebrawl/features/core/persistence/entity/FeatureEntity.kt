package xyz.levert.featurebrawl.features.core.persistence.entity

import org.bson.codecs.pojo.annotations.BsonId
import org.bson.types.ObjectId

data class FeatureEntity(
    @BsonId
    val id: ObjectId = ObjectId(),
    val name: String,
    val description: String?,
    val votes: List<VoteEntity> = emptyList(),
    val score: Int = 0
)
