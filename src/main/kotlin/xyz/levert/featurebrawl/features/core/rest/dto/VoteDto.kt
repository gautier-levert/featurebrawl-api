package xyz.levert.featurebrawl.features.core.rest.dto

import io.micronaut.core.annotation.Introspected
import javax.validation.constraints.Max
import javax.validation.constraints.Min

@Introspected
data class VoteDto(
    @field:Min(-5)
    @field:Max(5)
    val note: Int
)
