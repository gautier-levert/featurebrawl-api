package xyz.levert.featurebrawl.features.core.rest.dto

import io.micronaut.core.annotation.Introspected

@Introspected
data class FeatureDto(
    val id: String,
    val name: String = "",
    val description: String? = null,
    val score: Int = 0
)
