package xyz.levert.featurebrawl.features.core.rest

import io.micronaut.core.version.annotation.Version
import io.micronaut.http.HttpStatus
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.Post
import io.micronaut.http.exceptions.HttpStatusException
import io.micronaut.validation.Validated
import xyz.levert.featurebrawl.features.core.domain.FeatureService
import xyz.levert.featurebrawl.features.core.domain.data.Vote
import xyz.levert.featurebrawl.features.core.rest.dto.CreateFeatureDto
import xyz.levert.featurebrawl.features.core.rest.dto.FeatureDto
import xyz.levert.featurebrawl.features.core.rest.dto.VoteDto
import xyz.levert.featurebrawl.features.core.rest.mapping.FeatureDtoMapper
import xyz.levert.featurebrawl.features.commons.validation.ValidId
import javax.validation.Valid

@Controller("features")
@Version("1")
@Validated
class FeatureController(
    private val service: FeatureService,
    private val mapper: FeatureDtoMapper,
) {

    @Get
    fun findAll(): List<FeatureDto> {
        return service.findAll()
            .map(mapper::domainToDto)
    }

    @Get("{id}")
    fun findOne(@ValidId @PathVariable("id") id: String): FeatureDto {
        return service.findOne(id)
            ?.let { mapper.domainToDto(it) }
            ?: throw HttpStatusException(HttpStatus.NOT_FOUND, "No feature with id $id")
    }

    @Post
    fun create(@Valid @Body feature: CreateFeatureDto): FeatureDto {
        val domainObject = mapper.dtoToDomain(feature)
        return service.addFeature(domainObject)
            .let(mapper::domainToDto)
    }

    @Post("{id}/vote")
    fun create(@ValidId @PathVariable("id") id: String, @Valid @Body vote: VoteDto) {
        return service.addVote(id, Vote(note = vote.note))
    }
}
