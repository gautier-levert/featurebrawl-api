package xyz.levert.featurebrawl.features.core.persistence.entity

data class VoteEntity(
    val note: Int,
    val author: String
)
