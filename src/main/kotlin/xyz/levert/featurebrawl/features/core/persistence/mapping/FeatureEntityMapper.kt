package xyz.levert.featurebrawl.features.core.persistence.mapping

import org.bson.types.ObjectId
import org.mapstruct.CollectionMappingStrategy
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import xyz.levert.featurebrawl.features.core.domain.data.Feature
import xyz.levert.featurebrawl.features.core.persistence.entity.FeatureEntity

@Mapper(componentModel = "jsr330", collectionMappingStrategy = CollectionMappingStrategy.TARGET_IMMUTABLE)
interface FeatureEntityMapper {

    fun entityToDomain(entity: FeatureEntity): Feature

    @InheritInverseConfiguration
    fun domainToEntity(domain: Feature): FeatureEntity

    fun objectIdToString(id: ObjectId): String = id.toString()

    fun stringToObjectId(id: String) = ObjectId(id)
}
