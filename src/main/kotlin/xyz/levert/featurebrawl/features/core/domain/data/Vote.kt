package xyz.levert.featurebrawl.features.core.domain.data

data class Vote(
    val note: Int,
    val author: String = "Anonymous",
)
