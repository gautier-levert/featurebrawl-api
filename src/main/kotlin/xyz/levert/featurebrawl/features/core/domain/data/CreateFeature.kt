package xyz.levert.featurebrawl.features.core.domain.data

data class CreateFeature(
    val name: String,
    val description: String?,
)
