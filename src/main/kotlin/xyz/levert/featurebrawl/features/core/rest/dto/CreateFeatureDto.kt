package xyz.levert.featurebrawl.features.core.rest.dto

import io.micronaut.core.annotation.Introspected
import javax.validation.constraints.NotBlank

@Introspected
data class CreateFeatureDto(
    @field:NotBlank
    val name: String = "",
    val description: String? = null
)
