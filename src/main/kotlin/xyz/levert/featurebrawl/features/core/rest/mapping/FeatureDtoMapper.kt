package xyz.levert.featurebrawl.features.core.rest.mapping

import org.mapstruct.CollectionMappingStrategy
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import xyz.levert.featurebrawl.features.core.domain.data.CreateFeature
import xyz.levert.featurebrawl.features.core.domain.data.Feature
import xyz.levert.featurebrawl.features.core.rest.dto.CreateFeatureDto
import xyz.levert.featurebrawl.features.core.rest.dto.FeatureDto

@Mapper(componentModel = "jsr330", collectionMappingStrategy = CollectionMappingStrategy.TARGET_IMMUTABLE)
interface FeatureDtoMapper {

    fun domainToDto(domain: Feature): FeatureDto

    @InheritInverseConfiguration
    fun dtoToDomain(dto: FeatureDto): Feature

    fun dtoToDomain(dto: CreateFeatureDto): CreateFeature
}
