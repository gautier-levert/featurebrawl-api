package xyz.levert.featurebrawl.features.core.domain.data

data class Feature(
    val id: String,
    val name: String,
    val description: String?,
    val votes: List<Vote> = emptyList(),
) {
    val score: Int = votes.sumOf(Vote::note)
}
