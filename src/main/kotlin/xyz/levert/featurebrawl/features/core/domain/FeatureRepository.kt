package xyz.levert.featurebrawl.features.core.domain

import xyz.levert.featurebrawl.features.core.domain.data.CreateFeature
import xyz.levert.featurebrawl.features.core.domain.data.Feature
import xyz.levert.featurebrawl.features.core.domain.data.Vote

interface FeatureRepository {
    fun findAll(): List<Feature>
    fun findOne(id: String): Feature?
    fun insert(creation: CreateFeature): Feature
    fun addVote(id: String, vote: Vote)
}
