package xyz.levert.featurebrawl.features.core.domain

import xyz.levert.featurebrawl.features.core.domain.data.CreateFeature
import xyz.levert.featurebrawl.features.core.domain.data.Feature
import xyz.levert.featurebrawl.features.core.domain.data.Vote
import javax.inject.Singleton

@Singleton
class FeatureService(
    private val featureRepository: FeatureRepository
) {

    fun findAll(): List<Feature> {
        return featureRepository.findAll()
    }

    fun findOne(id: String): Feature? {
        return featureRepository.findOne(id)
    }

    fun addFeature(feature: CreateFeature): Feature {
        return featureRepository.insert(feature)
    }

    fun addVote(id: String, vote: Vote) {
        return featureRepository.addVote(id, vote)
    }
}
