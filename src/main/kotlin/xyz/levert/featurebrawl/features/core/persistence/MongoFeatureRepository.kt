package xyz.levert.featurebrawl.features.core.persistence

import com.mongodb.client.MongoDatabase
import org.bson.types.ObjectId
import org.litote.kmongo.combine
import org.litote.kmongo.descending
import org.litote.kmongo.findOneById
import org.litote.kmongo.getCollection
import org.litote.kmongo.inc
import org.litote.kmongo.push
import org.litote.kmongo.updateOneById
import xyz.levert.featurebrawl.features.core.domain.data.CreateFeature
import xyz.levert.featurebrawl.features.core.domain.data.Feature
import xyz.levert.featurebrawl.features.core.domain.FeatureRepository
import xyz.levert.featurebrawl.features.core.domain.data.Vote
import xyz.levert.featurebrawl.features.core.persistence.entity.FeatureEntity
import xyz.levert.featurebrawl.features.core.persistence.mapping.FeatureEntityMapper
import javax.inject.Singleton

@Singleton
class MongoFeatureRepository(
    database: MongoDatabase,
    private val mapper: FeatureEntityMapper
) : FeatureRepository {

    private val collection = database.getCollection<FeatureEntity>("features")

    override fun findAll(): List<Feature> {
        return collection.find()
            .sort(descending(FeatureEntity::score))
            .map(mapper::entityToDomain)
            .toList()
    }

    override fun findOne(id: String): Feature? {
        return collection.findOneById(ObjectId(id))
            ?.let(mapper::entityToDomain)
    }

    override fun insert(creation: CreateFeature): Feature {
        val entity = FeatureEntity(
            name = creation.name,
            description = creation.description,
        )
        collection.insertOne(entity)
        return mapper.entityToDomain(entity)
    }

    override fun addVote(id: String, vote: Vote) {
        collection.updateOneById(
            ObjectId(id),
            combine(inc(Feature::score, vote.note), push(Feature::votes, vote))
        )
    }
}
