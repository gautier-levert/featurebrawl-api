package xyz.levert.featurebrawl.features.commons.validation

import io.micronaut.core.annotation.AnnotationValue
import io.micronaut.validation.validator.constraints.ConstraintValidator
import io.micronaut.validation.validator.constraints.ConstraintValidatorContext
import org.bson.types.ObjectId
import javax.inject.Singleton

@Singleton
class IdConstraintValidator : ConstraintValidator<ValidId, String> {

    override fun isValid(
        value: String?,
        annotationMetadata: AnnotationValue<ValidId>,
        context: ConstraintValidatorContext
    ): Boolean {
        return value?.let { ObjectId.isValid(it) } ?: true
    }
}
