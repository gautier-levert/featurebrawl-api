package xyz.levert.featurebrawl.features.commons.validation

import javax.validation.Constraint

@Retention(AnnotationRetention.RUNTIME)
@Constraint(validatedBy = [])
annotation class ValidId(
    val message: String = "invalid id value `{validatedValue}`"
)
