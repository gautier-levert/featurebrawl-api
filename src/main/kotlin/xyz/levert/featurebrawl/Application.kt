package xyz.levert.featurebrawl

import io.micronaut.runtime.Micronaut.*

fun main(args: Array<String>) {
    build()
        .args(*args)
        .packages("xyz.levert.featurebrawl")
        .start()
}
